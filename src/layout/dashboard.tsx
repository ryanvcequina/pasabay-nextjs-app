
import { useRouter } from 'next/router'
import Button from '@/components/button/button'
interface DashboardProps {
    children?: JSX.Element[] | JSX.Element
  
  }
  
  
  const Dashboard = ({children}:DashboardProps)=> {
    const router = useRouter()


    const handleClick = (e:any,url:string)=>{ 
        e.preventDefault()
        console.log('hello')
        router.push(`/${url}`)
    }



      return (
        <div className="dashboard"> 
        <header className='dashboard-header'>header</header>
        <div className='dashboard-container'>
            <div className='dashboard-sidenav'>
                <button className='dashboard-sidenav__btn' onClick={(e)=>handleClick(e,'dashboard')} >
                dashboard
                </button>
                
            
                <button className='dashboard-sidenav__btn' onClick={(e)=>handleClick(e,'franchisee')}> 
                franchisee
                </button> 
            
            </div>
            <div className='dashboard-content'>
                <section>
                    {children}
                </section>

                <footer className='dashboard-footer'>footer</footer>
            </div>
        </div>
       
        </div>
      )
    }
    
export default Dashboard