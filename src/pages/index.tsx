import Image from 'next/image'
import { Inter } from 'next/font/google'
import Button from '@/components/button/button'
import { useRouter } from 'next/router'
const inter = Inter({ subsets: ['latin'] })

export default function Home() {
    const router = useRouter()
    // function handleClick(event: MouseEvent<HTMLButtonElement, MouseEvent>): void {
    //     throw new Error('Function not implemented.')
    // }
    const handleClick = ()=>{
        console.log('hello')
        router.push('/dashboard')
    }

  return (
       <div className='login'>
            <div className='login__wrapper'>
                <div className='login__image' />
            </div>

            <div className='login__wrapper'>

                <div className='login__form'>
                    {/* <img src={LOGO} alt='logo' /> */}
                    <Image
                        src="/images/pasabay-logo.png"
                        alt="food"
                        width={400}
                        height={200}
                      quality={100}
                      />
                    <div
                       
                        className='text-center mt-3 mb-5'>
                    </div>
          
                    <Button classname='login__btn' label={'go to dashboard'} onClick={handleClick}/>
                    {/* <Link to='/create-account'>Create an Account</Link> */}
                </div>
            </div>
      </div>
  )
}
